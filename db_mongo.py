from pymongo import MongoClient
from datetime import datetime
from logzero import logger


def get_database():
    connection_string = "mongodb://localhost:27017"
    client = MongoClient(connection_string)
    return client['news-box']


def save_links(source, links):
    database = get_database()
    coll_links = database.get_collection("links")
    for link in links:
        result = coll_links.replace_one(
            {"l": link},
            {"s": source, "l": link, "u": datetime.utcnow()},
            upsert=True
        )
        logger.debug(f"result: {result}, s:{source}, l:{link}")


def save_speech(id, speech_file):
    database = get_database()
    coll_links = database.get_collection("links")    
    result = coll_links.update_one(
            {"_id": id},
            {"$set": {"speech": speech_file, "u": datetime.utcnow()}},
            upsert=True
        )
    logger.debug(f"_id: {id}, s:{speech_file}, saved:{result}")

def get_links_for_content():
    database = get_database()
    coll_links = database.get_collection("links")
    cursor_links = coll_links.find({'c': {'$exists': False}})
    links = []
    for link in cursor_links:
        links.append(link)
    return links


def get_links_for_speech():
    database = get_database()
    coll_links = database.get_collection("links")
    cursor_links = coll_links.find({'c': {'$exists': True}, 'speech':{'$exists': False}})
    links = []
    for link in cursor_links:
        links.append(link)
    return links


def get_recent_speech():
    database = get_database()
    coll_links = database.get_collection("links")
    cursor_links = coll_links.find({'c': {'$exists': True}, 'speech':{'$exists': True}}).sort("u", -1).limit(60)
    links = []
    for link in cursor_links:
        links.append(link)
    return links


def save_link_content(link, content):
    database = get_database()
    coll_links = database.get_collection("links")
    result = coll_links.update_one(
        {"l": link},
        {"$set": {"c": content, "u": datetime.utcnow()}},
        upsert=True
    )
    logger.debug(f"link:{link} saved: {result}")
