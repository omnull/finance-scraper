from flask import Flask, request, send_from_directory
from flask_cors import CORS, cross_origin
from requests import get
import urllib.parse 
import web_meta
from logzero import logger
from decouple import config
import db_mongo
import json
import requests

host_addr = config('HOST_ADDR')
goplay_host = config('GOPLAY_HOST')

app = Flask(__name__, static_url_path='', static_folder='web/static')
CORS(app)


@app.route('/metadata/parse', methods=['POST'])
def metadata_parse():
    payload = request.get_json(True)
    wmp = web_meta.WebMetadataParser(payload["url"])
    meta = wmp.parse_metadata()
    if meta['image'] != "":
        decoded_image = urllib.parse.quote_plus(meta["image"])
        meta["image"] = f"{host_addr}/p?q={decoded_image}"
    return meta


@app.route('/p')
def proxy():
    return get(request.args.get('q')).content


@app.route('/speeches', methods=['GET'])
def speech():
    links = db_mongo.get_recent_speech()
    speeches = []
    for link in links:
        speeches.append( 
            {
                '_id':f"{link['_id']}", 
                'url': f"{host_addr}/{link['speech']}"
            } 
        )
    return {'speeches':speeches}


@app.route('/event/<slug>', methods=['GET'])
def event(slug):
    headers_dict = {"cookie": "gp_fgp=abc"}

    r =requests.get(f"{goplay_host}/api/v1/live/event/{slug}", headers=headers_dict)
    if r.status_code == 200:
        return json.loads(r.text)
    print("err:", r.status_code )
    return None