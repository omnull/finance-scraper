from selenium.webdriver.common.action_chains import ActionChains
from scrapy import Selector
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import selenium.common.exceptions as exception
from logzero import logger

user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'

def get_links(headless=True):
    if headless:
        options = webdriver.ChromeOptions()
        options.add_argument("headless")
        options.add_argument("no-sandbox")
        options.add_argument(f'user-agent={user_agent}')
        desired_capabilities = options.to_capabilities()
        driver = webdriver.Chrome(desired_capabilities=desired_capabilities)
    else:
        driver = webdriver.Chrome()
    
    driver.get("https://finansial.bisnis.com/")

    wait = WebDriverWait(driver, 15)
    wait.until(EC.presence_of_element_located((By.XPATH, '//div[@class="btn-loadmore"]')))

    e_articles = driver.find_elements_by_xpath('//ul[@class="list-news"]//div[@class="col-sm-8"]/h2/a')
    links = []
    for e_article in e_articles:
        link = e_article.get_attribute('href')
        links.append(link)

    driver.close()

    return links


def load_detail(link, headless=True):
    if headless:
        options = webdriver.ChromeOptions()
        options.add_argument("headless")
        options.add_argument("no-sandbox")
        options.add_argument(f'user-agent={user_agent}')
        desired_capabilities = options.to_capabilities()
        driver = webdriver.Chrome(desired_capabilities=desired_capabilities)
    else:
        driver = webdriver.Chrome()

    driver.get(link)

    try:
        wait = WebDriverWait(driver, 15)
        wait.until(EC.presence_of_element_located((By.XPATH, '//div[@class="editor"]')))
        selenium_response_text = driver.page_source
        return selenium_response_text
    except exception.TimeoutException as e:
        logger.debug(f"Timeout on link:{link}, e:{e}")
    except exception.NoSuchElementException:
        logger.debug(f"NoSuchElementException on link:{link}, e:{e}")
    except exception.WebDriverException as e:
        logger.debug(f"WebDriverException on link:{link}, e:{e}")
    finally:        
        driver.close()

    return False    


def parse_info(response_text):
    selector = Selector(text=response_text)
    detail_texts = selector.xpath('//div[@itemprop="articleBody"]/p//text()').getall()
    return "\n".join(detail_texts)
