from google.cloud import texttospeech
import random
from logzero import logger

def to_speech(id, textInput):
    client = texttospeech.TextToSpeechClient()
    synthesis_input = texttospeech.SynthesisInput(text=textInput)


    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    names = ['id-ID-Standard-A', 'id-ID-Standard-B', 'id-ID-Standard-C', 'id-ID-Standard-D']
    name = names[random.randint(0, len(names)-1)]
    voice = texttospeech.VoiceSelectionParams(
        language_code="	id-ID", name=name
    )

    # Select the type of audio file you want returned
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.MP3
    )

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )

    # The response's audio_content is binary.
    speech_file = f"{id}.mp3"
    with open(f"./web/static/{speech_file}", "wb") as out:
        out.write(response.audio_content)
        logger.debug(f"Audio content written to file {speech_file}")

    return speech_file