# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import cnbc_indonesia
import finansial_bisnis
import db_mongo
import web_meta
from logzero import logger
import texttospeech
from bson.objectid import ObjectId

def collect_links():
    links = cnbc_indonesia.get_links()
    logger.debug("Links from cnbc:", links)
    db_mongo.save_links("cnbc_indonesia", links)

    links = finansial_bisnis.get_links()
    logger.debug("Links from finansial_bisnis:", links)
    db_mongo.save_links("finansial_bisnis", links)


def collect_content():
    dict_links = db_mongo.get_links_for_content()
    for dict_link in dict_links:
        logger.debug(f"link:{dict_link}")
        if dict_link['s'] == 'cnbc_indonesia':
            link = dict_link['l']
            html_detail = cnbc_indonesia.load_detail(link)
            if html_detail:
                detail_text = cnbc_indonesia.parse_info(html_detail)
                db_mongo.save_link_content(link, detail_text)
        elif dict_link['s'] == 'finansial_bisnis':
            link = dict_link['l']
            html_detail = finansial_bisnis.load_detail(link)
            if html_detail:
                detail_text = finansial_bisnis.parse_info(html_detail)
                db_mongo.save_link_content(link, detail_text)


def collect_speech():
    dict_links = db_mongo.get_links_for_speech()
    for dict_link in dict_links:
        logger.debug(f"link:{dict_link}")
        if dict_link['c'] == "" or len(dict_link['c']) >= 5000 :
            continue
        speech_file = texttospeech.to_speech(dict_link['_id'], dict_link['c'])
        db_mongo.save_speech(dict_link['_id'], speech_file)


if __name__ == '__main__':
    collect_links()
    collect_content()
    collect_speech()

    #https://www.instagram.com/coldplay/
    #wmp = web_meta.WebMetadataParser('https://docs.google.com/document/d/1Y-zJMRgzmqeMiq12rRUDLoHlBprS263zvnw6vbq7E0Q/edit')
    #meta = wmp.parse_metadata()
    #print(meta)