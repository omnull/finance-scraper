import requests
from scrapy import Selector
from logzero import logger
from selenium import webdriver

class WebMetadataParser():

    def __init__(self, url):
        self.url = url    

    def start_requests(self):
        logger.debug(f"start_request:{self.url}")
        headers_dict = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
            'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
            'sec-ch-ua-platform': "macOS"
        }

        try:
            user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
            options = webdriver.ChromeOptions()
            options.add_argument("headless")
            options.add_argument("no-sandbox")
            options.add_argument(f'user-agent={user_agent}')
            desired_capabilities = options.to_capabilities()
            driver = webdriver.Chrome(desired_capabilities=desired_capabilities)
            driver.get(self.url)
            self.response_text = driver.page_source
            self.selector = Selector(text=self.response_text)
            return True
        except Exception as e:
             logger.debug(f"error:{e}")
             return False
        finally:
            driver.close()

        r =requests.get(self.url, headers=headers_dict)
        if r.status_code == 200:    
            self.response_text = r.text
            self.selector = Selector(text=self.response_text)
            return True
        else:
            logger.debug(f"error:{r.status_code}")

        return False


    def parse_metadata(self):
        if not self.start_requests():
            return False        
        
        title = self.select_attrib('/html/head/meta[@property="og:title"]', 'content')
        if title == "":
            title = self.selector.xpath('/html/head/title/text()').getall()
            title = "\n".join(title)
        description = self.select_attrib('/html/head/meta[@property="og:description"]', 'content')
        image = self.select_attrib('/html/head/meta[@property="og:image"]', 'content')
        return {"title":title, "description": description, "image": image}


    def select_attrib(self, query, attrib):
        elem = self.selector.xpath(query)
        if len(elem) > 0:
            return elem.attrib[attrib]
        return ""

